# MatDown Tutorial
## Version: 0.12

MatDown is a text-based, readable formatting syntax that extends [Markdown](https://daringfireball.net/projects/markdown/) and at times looks similar to [Python](http://python.org).
MatDown turns plain text files into _consistent_ looking documents, resumes, slide decks, etc., allowing you to focus on content rather than formatting.

MatDown boasts many powerful features.
The first feature is the ability to run in _any_ web browser.
Many of the other features are discussed and demonstrated in this tutorial.

Source: [link](https://gfx.cse.taylor.edu/matdown/tutorial.md)  
Render: [link](https://gfx.cse.taylor.edu/matdown/tutorial.md.html)

Note: this tutorial (and MatDown) is a work in progress!
Use at your own risk!



### Math Stuff

MatDown uses [MathJax](http://mathjax.org) to render beautiful looking math.
Here is some inline math $x=\frac{1}{2}$, and here is some display math.

$$x = \frac{1}{2}$$



### Source code stuff

You can use <code>```</code> or <code>~~~</code> for source code fences.
TODO: <code>```prettyprint linenums python</code> should work, but does not.

```
sample = 'foo bar'

def foo():
    print('test')
    # ^^test^^ **test** _test_

foo()
```


~~~
var x = function() { };
~~~


### Enumerations

Below is an ordered list starting with 11.

~~~~
11. &lt;span style="background-color:yellow">an&lt;/span>
2. ^ordered^
3. ^^list^^
4. ^^^of fun^^^
~~~~

11. <span style="background-color:yellow">an</span>
2. ^ordered^
3. ^^list^^
4. ^^^of fun^^^

Below is another ordered list.

a. something
b. but not here
c. or here


A nested list?

1. one
    a. one.one
    b. one.two
2. two
    - bulleted
    - inside
3. three

Below is an unordered list.

- an
    unordered
    list
- foo
- bar



### Images

![euler](euler-winter-2.jpg w=100px)
Inlined images with captions automatically float to the right (TODO: fix!!).
Inlined images with no captions, such as ![](euler-winter-2.jpg h=10px), remain inline.
Isn't that great?

When an image is in its own paragraph, then the image stands by itself.
The image is contained in a figure if it has a caption.

![euler](euler-winter-2.jpg w=100px)

![](euler-winter-2.jpg w=100px)


A sequence of captionless images without text _should_ go side-by-side.

![](euler-winter-2.jpg w=100px)
![](euler-winter-2.jpg w=100px)

A sequence of captioned images without text _should_ float right.

![fig: euler science complex](euler-winter-2.jpg w=100px)
![fig: euler science complex](euler-winter-2.jpg w=100px)


### Quotes

This is a quote

> to be or not to be?
> is that the question?
