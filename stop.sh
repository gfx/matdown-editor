#!/bin/bash -e

# ps ax            reports all running processes
# grep matdown.py  looks for processes with matdown.py
# awk '{print $1}' removes all but first column (contains pid)
# head -n -1       removes the last line (which is for the ps ax)
# tr '\n' ' '      concats all lines to single line

PIDS=`ps ax | grep matdown.py | awk '{print $1}' | head -n -1 | tr '\n' ' '`

if [ ! -z "$PIDS" ]; then
    echo "Stopping MatDown process"
    kill -9 $PIDS
else
    echo "No MatDown processes running"
fi

