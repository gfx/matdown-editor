from flask import Flask
from flask import send_from_directory
from flask import request, redirect, session
from flask import make_response
from functools import wraps, update_wrapper
from datetime import datetime
import subprocess
import os
import md5 as MD5
import datetime
import random

from flask_socketio import SocketIO


if not os.path.exists('secretkey.txt'):
    secretKey = MD5.new(str(random.random())).hexdigest()
    open('secretkey.txt','wt').write(secretKey)
else:
    secretKey = open('secretkey.txt','rt').read()

timestampExprire = datetime.timedelta(days=2)


def genTimestamp():
    global secretKey
    dt = str(datetime.date.today())
    h = MD5.new(dt + secretKey).hexdigest()
    return '%s %s' % (dt,h)

def validateTimestamp():
    global secretKey, timestampExprire
    if 'authenticated' not in session: return False
    ts = session['authenticated']
    if ' ' not in ts: return False
    dt,h = ts.split(' ')
    h_ = MD5.new(dt + secretKey).hexdigest()
    if h != h_: return False
    dt = map(int, dt.split('-'))
    d = datetime.date(*dt)
    n = datetime.date.today()
    diff = n - d
    if diff > timestampExprire: return False
    # XXX: what if diff < 0?
    session['authenticated'] = genTimestamp()   # re-authenticate
    return True

def isUserLoggedIn():
    return validateTimestamp()

def logUserIn(username, password):
    # yes, this is terrible and bad.  this needs to be fixed!
    # flask has a nice login manager.  why not use it?
    if not os.path.exists('users.txt'):
        # create a default username/password db
        users = {
            'user1': 'password',
            'user2': 'password'
        }
        users = '\n'.join('%s,%s' % (u,p) for u,p in users.iteritems())
        open('users.txt','wt').write(users)
    users = open('users.txt').read().splitlines()
    users = [l.split(',') for l in users]
    users = {u:p for u,p in users}
    if username is None or password is None: return False
    if username not in users: return False
    if users[username] != password: return False
    session['authenticated'] = genTimestamp()
    return True



class Logger():
    def __init__(self, path, basename):
        self.logfilename = os.path.join(path, '%s.report' % basename)
        if not os.path.exists(self.logfilename):
            self.logfile = open(self.logfilename, 'wt')
        else:
            self.logfile = open(self.logfilename, 'at')
        self.logfile.write('--------------\n')
    def log(self, st):
        self.logfile.write('%s\n' % st)


# http://code.tutsplus.com/tutorials/creating-a-web-app-from-scratch-using-python-flask-and-mysql--cms-22972

# http://arusahni.net/blog/2014/03/flask-nocache.html
# does not work?
def nocache(view):
    @wraps(view)
    def no_cache():
        response = make_response(view(*args, **kwargs))
        response.headers['Last-Modified'] = datetime.now()
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response
        
    return update_wrapper(no_cache, view)

app = Flask(__name__)
app.secret_key = secretKey
socketio = SocketIO(app)

@app.route('/matdown/')
def index():
    qs = '?%s' % request.query_string if request.query_string else ''
    return redirect("matdown/tutorial.md.html%s" % qs)

#################################################################################
#################################################################################


@socketio.on_error_default
def handle_error(e):
    pass

@socketio.on('connect')
def handle_connect():
    pass

@socketio.on('disconnect')
def handle_disconnect(data):
    pass

@socketio.on('deluser')
def handle_deluser(data):
    socketio.emit('deluser', data, broadcast=True)

@socketio.on('cursor')
def handle_cursor(data):
    if not isUserLoggedIn(): return False
    socketio.emit('cursor', data, broadcast=True)

@socketio.on('update')
def handle_update(data):
    # valid user?
    if not isUserLoggedIn():
        return False
    
    # path good?
    # this is not good!
    path = data.get('file', None).strip()
    if path is None :
        socketio.emit('error', {'message':'no path'})
        return 'no path'
    if '..' in path:
        socketio.emit('error', {'message':'bad path'})
        return 'bad path'
    path = os.path.join('static', path)
    if not os.path.abspath(path).startswith(os.path.abspath('static')):
        socketio.emit('error', {'message':'bad path'})
        return 'bad path'
    if not os.path.exists(path):
        socketio.emit('error', {'message':'file not found'})
        return 'file not found'
    
    # load base file
    basefile = open(path, 'rt').read()
    
    # is base same?
    md5prev = MD5.new(basefile).hexdigest()
    if data.get('md5previous', None) != md5prev:
        socketio.emit('error', {'message':'bad md5'})
        return 'bad md5'
    
    # perform update
    idx = data.get('index', None)
    lp = data.get('lenprevious', None)
    lc = data.get('lencurrent', None)
    ta = data.get('add', None)
    td = data.get('del', None)
    if idx is None or lp is None or lc is None or ta is None or td is None:
        socketio.emit('error', {'message':'bad update: None'})
        return 'bad update: None'
    if basefile[idx:idx+lp] != td:
        socketio.emit('error', {'message':'bad update: diff'})
        return 'bad update: diff'
    newfile = basefile[:idx] + ta + basefile[idx+lp:]
    open(path, 'wt').write(newfile)
    
    socketio.emit('update', data, broadcast=True)
    return 'ok'


@app.route('/matdown/login', methods=["POST"])
def login():
    data = request.json
    logUserIn(data.get('username',''), data.get('password',''))
    if isUserLoggedIn(): return 'ok'
    return 'nope'

@app.route('/matdown/amiloggedin')
def amILoggedIn():
    if isUserLoggedIn(): return 'ok'
    return 'nope'

#################################################################################
#################################################################################


@app.route('/matdown/<path:path>/<string:basename>.md.html')
@app.route('/matdown/<string:basename>.md.html', defaults={'path':''})
def gethtml(path, basename):
    path = os.path.join('static', path)
    md = '%s.md' % basename
    md5 = '%s.md5' % basename
    html = '%s.html' % basename
    report = '%s.report' % basename
    
    log = Logger(path, basename).log
    log('>>> getting HTML')
    log('>>> query: %s' % request.query_string) # request.args.get('debug',False))
    
    # process
    pathcur = os.path.abspath(os.getcwd())
    log('>>> Working path: %s' % path)
    log('>>> md: %s' % md)
    os.chdir(path)
    
    if not os.path.exists('template-doc'):
        os.symlink('/home/jdenning/projects/common/template-doc', 'template-doc')
    if not os.path.exists('template-slide'):
        os.symlink('/home/jdenning/projects/common/template-slide', 'template-slide')
    
    mddata = open(md,'rt').read()
    md5prev = open(md5,'rt').read() if os.path.exists(md5) else ''
    md5new = MD5.new(mddata).hexdigest()
    
    if 'nocache' in request.query_string:
        md5prev = '--%s--' % md5prev
    
    log('>>> MD5: %s %s' % (md5prev, md5new))
    
    if md5prev != md5new:
        cmdpython = '/usr/bin/python'
        cmdbuild = '/home/jdenning/projects/common/build-doc/assignment/build.py'
        #cmdbuild = '/home/jdenning/projects/common/build-slide/slides/build.py'
        cmd = '%s %s %s' % (cmdpython,cmdbuild,md)
        
        log('>>> Processing')
        freport = open(report,'wt')
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        for line in proc.stdout.readlines():
            freport.write(line)
        freport.close()
        retval = proc.wait()
        
        log('>>> Return: ' + str(retval))
        open(md5,'wt').write(md5new)
    else:
        log('>>> Returning cache!')
    
    os.chdir(pathcur)
    
    return send_from_directory('', os.path.join(path,html))


@app.route('/matdown/<path:path>/<string:basename>.md')
@app.route('/matdown/<string:basename>.md', defaults={'path':''})
def getmd(path, basename):
    Logger('', 'matdown').log('>>> getting MD')
    md = os.path.join(path, '%s.md' % basename)
    return send_from_directory('static', md, mimetype="text")

@app.route('/matdown/<path:path>/<string:basename>.md.report')
@app.route('/matdown/<string:basename>.md.report', defaults={'path':''})
def getreport(path, basename):
    Logger('', 'matdown').log('>>> getting report')
    report = os.path.join(path, '%s.report' % basename)
    return send_from_directory('static', report, mimetype="text")


@app.route('/matdown/<path:filepath>')
def fileserve(filepath):
    Logger('', 'matdown').log('>> serving file: %s' % filepath)
    return send_from_directory('static', filepath)

#@app.route('/<us>/<cl>')
#def hello(us, cl):
#    return 'User: %s<br>Class: %s<br>' % (us, cl)

if __name__ == '__main__':
    #app.run(debug=True, host='0.0.0.0', port=5000)
    socketio.run(app, debug=True, host='0.0.0.0', port=5000)
